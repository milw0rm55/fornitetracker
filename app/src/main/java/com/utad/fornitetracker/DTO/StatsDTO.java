package com.utad.fornitetracker.DTO;

public class StatsDTO {
    public StatsDTO(P2DTO p2) {
        this.p2 = p2;
    }

    public P2DTO getP2() {
        return p2;
    }
    P2DTO p2;
}
