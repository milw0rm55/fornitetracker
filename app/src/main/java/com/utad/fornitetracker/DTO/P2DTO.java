package com.utad.fornitetracker.DTO;

public class P2DTO {
    public P2DTO(ParametrosDTO score, ParametrosDTO winRatio, ParametrosDTO kills, ParametrosDTO kpg) {
        this.score = score;
        this.winRatio = winRatio;
        this.kills = kills;
        this.kpg = kpg;
    }

    public ParametrosDTO getScore() {
        return score;
    }

    public ParametrosDTO getWinRatio() {
        return winRatio;
    }

    public ParametrosDTO getKills() {
        return kills;
    }

    public ParametrosDTO getKpg() {
        return kpg;
    }

    ParametrosDTO score;
    ParametrosDTO winRatio;
    ParametrosDTO kills;
    ParametrosDTO kpg;
}
