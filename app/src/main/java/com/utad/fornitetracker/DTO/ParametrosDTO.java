package com.utad.fornitetracker.DTO;

public class ParametrosDTO {
    private String label;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getRank() {
        return rank;
    }

    private String value;

    public ParametrosDTO(String label, String value, String rank) {
        this.label = label;
        this.value = value;
        this.rank = rank;
    }

    private String rank;
}
