package com.utad.fornitetracker.DTO;

public class JugadorDTO {
    public JugadorDTO(StatsDTO stats) {
        this.stats = stats;
    }

    public StatsDTO getStats() {
        return stats;
    }

    StatsDTO stats;
}