package com.utad.fornitetracker.Activies;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import com.utad.fornitetracker.Adapters.StatsAdapter;
import com.utad.fornitetracker.DTO.ParametrosDTO;
import com.utad.fornitetracker.R;
import com.utad.fornitetracker.ViewModel.StatsViewModel;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ArrayList<ParametrosDTO> parameters;
    private FloatingActionButton btnSearch;
    private EditText etUserForSearch;
    private Spinner sp_platform;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recyclerView_main);
        parameters = new ArrayList<>();
        etUserForSearch = findViewById(R.id.editText_search);
        btnSearch = findViewById(R.id.btn_search);
        sp_platform = findViewById(R.id.spinner_platform);


        StatsViewModel statsViewModel = ViewModelProviders.of(this).get(StatsViewModel.class);
        statsViewModel.getParametersDataMutableLiveData().observe(this, arr_params -> {
            if (arr_params != null) {

                parameters = arr_params;


                recyclerView.setLayoutManager(new GridLayoutManager(MainActivity.this, 2));
                recyclerView.setAdapter(new StatsAdapter(parameters));

            }


        });
        statsViewModel.getData("pc","");

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                parameters.clear();
                statsViewModel.getData(sp_platform.getSelectedItem().toString().trim(), etUserForSearch.getText().toString());

            }
        });


    }
}
