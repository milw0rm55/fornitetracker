package com.utad.fornitetracker;


import com.utad.fornitetracker.DTO.JugadorDTO;
import io.reactivex.Flowable;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

public interface ServiceInterfaz {

    // GET https://api.fortnitetracker.com/v1/profile/{platform}/{epic-nickname}

    @Headers({"TRN-Api-Key: 74d133f5-620d-4238-acfa-4b48b0bab454"})
    @GET("profile/{platform}/{epic-nickname}")
    Flowable<JugadorDTO> loadPlayer(@Path("platform") String platform, @Path("epic-nickname") String nickname);

    static ServiceInterfaz create() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.fortnitetracker.com/v1/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(ServiceInterfaz.class);
    }

}