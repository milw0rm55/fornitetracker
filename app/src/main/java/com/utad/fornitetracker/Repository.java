package com.utad.fornitetracker;

import com.utad.fornitetracker.DTO.JugadorDTO;
import io.reactivex.Flowable;

public class Repository {
    private static Repository instance;
    private Repository() {
    }

    public static Repository getInstance() {
        if (instance == null){
            synchronized (Repository.class){
                if (instance == null){
                    instance = new Repository();
                }
            }
        }
        return instance;
    }

    private ServiceInterfaz apiService = ServiceInterfaz.create();

    public Flowable<JugadorDTO> getPlayerData(String platform, String nickname) {
        return apiService.loadPlayer(platform,nickname);
    }


}