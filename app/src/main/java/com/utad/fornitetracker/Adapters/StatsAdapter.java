package com.utad.fornitetracker.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.utad.fornitetracker.DTO.ParametrosDTO;
import com.utad.fornitetracker.R;
import com.utad.fornitetracker.ViewHolder.ViewHolder;

import java.util.ArrayList;

public class StatsAdapter extends RecyclerView.Adapter<ViewHolder> {
    private ArrayList<ParametrosDTO> parameters;

    public StatsAdapter(ArrayList<ParametrosDTO> parameters) {
        this.parameters = parameters;


    }

    public void setData(ArrayList<ParametrosDTO> parameters) {
        this.parameters = parameters;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return parameters.size();

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.stats_cardview_item, null);
        ViewHolder mainCellViewHolder = new ViewHolder(view);
        return mainCellViewHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder mainCellViewHolder, int position) {

        ParametrosDTO item = parameters.get(position);
        if (item.getLabel() != null) {
            mainCellViewHolder.FirstTextView.setText(item.getLabel());
        } else {
            mainCellViewHolder.FirstTextView.setText("error");

        }
        if (item.getRank() != null) {
            mainCellViewHolder.SecondTextView.setText("Ranking: " + item.getRank());
        } else {
            mainCellViewHolder.SecondTextView.setText("error");

        }
        if (item.getValue() != null) {
            mainCellViewHolder.ThirdTextView.setText("Precisión: " + item.getValue());
        } else {
            mainCellViewHolder.ThirdTextView.setText("error");

        }}

}