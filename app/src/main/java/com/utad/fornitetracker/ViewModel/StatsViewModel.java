package com.utad.fornitetracker.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;
import com.utad.fornitetracker.DTO.JugadorDTO;
import com.utad.fornitetracker.DTO.ParametrosDTO;
import com.utad.fornitetracker.Repository;
import io.reactivex.schedulers.Schedulers;

import java.util.ArrayList;

public class StatsViewModel extends AndroidViewModel {


    static final String BASE_URL = "https://git.eclipse.org/r/";
    private MutableLiveData<JugadorDTO> playerDataMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<ArrayList<ParametrosDTO>> parametersDataMutableLiveData = new MutableLiveData<>();
    private Repository repository = Repository.getInstance();
    private ArrayList<ParametrosDTO> arr_parameters = new ArrayList<>();


    public StatsViewModel(@NonNull Application application) {
        super(application);
    }


    public MutableLiveData<ArrayList<ParametrosDTO>> getParametersDataMutableLiveData() {
        return parametersDataMutableLiveData;
    }


    public MutableLiveData<JugadorDTO> getServiceDataMutableLiveData() {
        return playerDataMutableLiveData;
    }


    public void getData(String platform, String user) {
        repository.getPlayerData(platform, user)
                .subscribeOn(Schedulers.io())
                .subscribe(response -> {
                            arr_parameters.add(response.getStats().getP2().getWinRatio());
                            arr_parameters.add(response.getStats().getP2().getScore());
                            arr_parameters.add(response.getStats().getP2().getKpg());
                            arr_parameters.add(response.getStats().getP2().getKills());
                            parametersDataMutableLiveData.postValue(arr_parameters);
                        },
                        error -> Log.d("SERVICE RESPONSE", error.getLocalizedMessage()));


    }
}