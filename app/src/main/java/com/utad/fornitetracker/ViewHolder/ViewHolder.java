package com.utad.fornitetracker.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import com.utad.fornitetracker.R;

public class ViewHolder extends RecyclerView.ViewHolder {

    public TextView FirstTextView;
    public TextView SecondTextView;
    public TextView ThirdTextView;

    public ViewHolder(@NonNull View itemView) {
        super(itemView);
        FirstTextView = itemView.findViewById(R.id.title);
        SecondTextView = itemView.findViewById(R.id.first_TextView);
        ThirdTextView = itemView.findViewById(R.id.secondTextView);




    }


}
